var marker = null;
$(document).ready( function() {
  $('#post_form').on('change','.file', function () {
    $('.files').append('<input type="file" name="file[]" class="file">');
  });
  initialize();
});

function initialize() {
  var initPos = new google.maps.LatLng(
    37.4660847,
    126.9597142
  );
  var mapOptions = {
    center: initPos,
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(
    document.getElementById("admin_story_map"),
    mapOptions
  );

  google.maps.event.addListener(map, 'click', function(e) {
    clearMarker();

    lat = e.latLng.lat();
    lng = e.latLng.lng();

    $('.lat').val(lat);
    $('.lng').val(lng);

    var pos = new google.maps.LatLng(lat,lng);
    marker = new google.maps.Marker({
      position: pos
    });

    marker.setMap(map);
  });
}
function clearMarker() {
  if(marker != null) {
    marker.setMap(null);
  }
}
