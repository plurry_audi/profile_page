var needToConfirm = true;
var categories = null;
var files = {};
var empty_file = null;
var file_counter = 0;
$(document).ready(function () {
  setCategories();
  $('#major_selector').change(changeMinorCategory);
  
  $('.upload-controller').click(function() {
    empty_file.click();
  });

  $('.files').on('change','.file', function() {
    files[file_counter] = empty_file;
    file_counter += 1;
    addEmptyFileTag();
    refreshFileItems();
  });
  $('.files-wrap').on('click', '.file-remove', function() {
    var $file_item = $(this).parent();
    var file_index = $file_item.data('index');
    //remove hidden file tag
    removeFileTag(file_index);
    //remove file object
    removeFileObject(file_index);

    refreshFileItems();
  });

  
  addEmptyFileTag();
  $('.post-save').click(function() {
    needToConfirm = false;
  });

});

function removeFileTag(file_index) {
  $('#file'+file_index).remove();
}
function removeFileObject(file_index) {
  delete files[file_index];
}
function refreshFileItems() {
  clearFileItems();

  var count = 1;
  $.each( files, function(index, file) {
    $('.files-list').append(getNewFileItem(index, file, count));
    count++;
  });
  
}

function getNewFileItem(index, file, count) {
  var $file_item = $('<div/>');
  $file_item.addClass('file-item');
  $file_item.data('index', index);

  $file_item.append(count+". "+file[0].files[0].name);
  $file_item.append(getNewRemoveTag());

  return $file_item;
}
function getNewRemoveTag() {
    var $remove = $('<a/>');
    $remove.addClass('file-remove');
    $remove.addClass('btn');
    $remove.addClass('btn-danger');
    $remove.html('delete');

    return $remove;
}
function clearFileItems() {
  $('.files-list').html('');
}
function addEmptyFileTag() {
  var new_tag = getNewFileTag()
  $('.files').append(new_tag);
  empty_file = new_tag;
}

function getNewFileTag() {
  var $file = $('<input/>')
  $file.attr('name','files[]');
  $file.attr('type','file');
  $file.attr('id', 'file'+file_counter);
  $file.addClass("file");

  return $file;
}

function setCategories() {
  $.ajax({
    url: '/admin/post.json',
    type: 'get',
    success: function( response ) {
      categories = response
      changeMinorCategory();
    }
  });
}
function changeMinorCategory() {
  $('#minor_selector').text('');
  var major = $('#major_selector').val();
  $.each(categories[major], function(key,val) {
    $('#minor_selector')
      .append($('<option></option>')
      .attr('value',val)
      .text(val));
  });

}

window.onbeforeunload = function(e) {
  if( needToConfirm ) {
    var message = "정말 나가시겠습니까? 포스트는 삭제됩니다.";
    e.returnValue = message;
    return message;
  }
};

window.onunload = leave;
function leave() {
  $.ajax({
    type: 'get',
    url: '/admin/post/leave',
    async: false,
    success: function(response) {
    }
  });
}

