var categories = null;
var files = {};
var empty_file = null;
var file_counter = 0;
$(document).ready(function () {
  setCategories();
  $('#major_selector').change(changeMinorCategory);
  $('.upload-controller').click(function(e) {
    empty_file.click();
  });

  $('.files').on('change','.file', function() {
    files[file_counter++] = empty_file;
    addEmptyFileTag();;
    refreshFileItems();
  });
  $('.files-wrap').on('click', '.file-remove', function() {
    var $file_item = $(this).parent();
    var file_index = $file_item.data('index');
    if( $file_item.data('file-type') == 'new' ) {
      removeFileTag(file_index);
    } else if( $file_item.data('file-type') == 'exist' ) {
      setDeleteTag(file_index, $file_item);
    }
    //remove file
    removeFileObject(file_index);

    refreshFileItems();
  });

  
  loadExistImageData();
});
function loadExistImageData() {
  $.ajax({
    url: '/admin/post/modify/images',
    type: 'get',
    success: function( response ) {
      $.each( response, function(index, file) {
        var exist_tag = getExistFileTag(file);
        files[file_counter] = exist_tag;
        $('.files').append(exist_tag);
        file_counter++;
      });
      addEmptyFileTag();
      refreshFileItems();
    }
  });
}

function getExistFileTag(file) {
  var $file = $('<input/>')
  $file.attr('name','delete[]');
  $file.attr('type','hidden');
  $file.attr('id', 'delete'+file_counter);

  $file.data('file-name', file.name );
  $file.data('file-id', file.id);
  $file.addClass("file");

  return $file;
}
function setDeleteTag(file_index, $file_item) {
  $('#delete'+file_index).val($file_item.data('file-id'));
}
function removeFileTag(file_index) {
  $('#file'+file_index).remove();
}
function removeFileObject(file_index) {
  delete files[file_index];
}
function refreshFileItems() {
  clearFileItems();

  var count = 1;
  $.each( files, function(index, file) {
    $('.files-list').append(getFileItem(index, file, count));
    count++;
  });
  
}

function getFileItem(index, file, count) {
  var $file_item = $('<div/>');
  $file_item.addClass('file-item');
  $file_item.data('index', index);
  
  if(file.attr('type') == 'file') {
    $file_item.append(count+". "+file[0].files[0].name);
    $file_item.data('file-type', 'new');
  } else {
    $file_item.append(count+". "+file.data('file-name'));
    $file_item.data('file-type', 'exist');
    $file_item.data('file-id', file.data('file-id'));
  }

  $file_item.append(getNewRemoveTag());

  return $file_item;
}
function getNewRemoveTag() {
    var $remove = $('<a/>');
    $remove.addClass('file-remove');
    $remove.addClass('btn');
    $remove.addClass('btn-danger');
    $remove.html('delete');

    return $remove;
}
function clearFileItems() {
  $('.files-list').html('');
}
function addEmptyFileTag() {
  var new_tag = getNewFileTag()
  $('.files').append(new_tag);
  empty_file = new_tag;

}

function getNewFileTag() {
  var $file = $('<input/>')
  $file.attr('name','files[]');
  $file.attr('type','file');
  $file.attr('id', 'file'+file_counter);
  $file.addClass("file");

  return $file;
}
function setCategories() {
  $.ajax({
    url: '/admin/post/modify.json',
    type: 'get',
    success: function( response ) {
      categories = response
    }
  });
}
function changeMinorCategory() {
  $('#minor_selector').text('');
  var major = $('#major_selector').val();
  $.each(categories[major], function(key,val) {
    $('#minor_selector')
      .append($('<option></option>')
      .attr('value',val)
      .text(val));
  });

}
