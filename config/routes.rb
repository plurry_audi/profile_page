Rails.application.routes.draw do
  get '/' => 'home#index'
  get '/about' => 'about#index'
  get '/contact' => 'contact#index'
  post '/contact' => 'contact#send_mail'
  get '/story' => 'story#index'
  get '/blog' => 'blog#index'
  get '/blog/:pid' => 'post#index'
  get '/portfolio' => 'portfolio#index'
  get '/portfolio/:pid' => 'post#index'
  post '/comment' => 'post#comment_write'

  get '/admin' => 'admin#index'
  get '/admin/login' => 'admin#login'
  post '/admin/login' => 'admin#login_check'
  delete '/admin/logout' => 'admin#logout'
  get '/admin/story' => 'admin#story'
  post '/admin/story' => 'admin#story_proc'
  delete '/admin/story' => 'admin#story_delete'

  get '/admin/post' => 'admin#post'
  post '/admin/post' => 'admin#post_proc'
  get '/admin/post/leave' => 'admin#clean_post_image'
  delete '/admin/post/delete' => 'admin#post_delete'
  get '/admin/post/modify' => 'admin#post_modify'
  post 'admin/post/modify' => 'admin#post_modify_proc'
  get '/admin/post/modify/images' => 'admin#post_modify_get_images'

  #tinymce image upload
  post '/tinymce_assets' => 'tinymce_assets#create'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
