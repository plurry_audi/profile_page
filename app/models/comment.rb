class Comment < ActiveRecord::Base
  def date
    self.created_at.localtime.strftime("%B %d, %Y AT %H:%M %p")
  end
end
