class Category < ActiveRecord::Base
  has_many :posts
  has_many :minor, class_name: "Category"
  belongs_to :major, class_name: "Category", foreign_key: "category_id"

  def self.portfolio_categories
    category_id = Category.where(name: "Portfolio").take.id
    Category.where(category_id: category_id )
  end

  def self.blog_categories
    category_id = Category.where(name: "Blog").take.id

    Category.where(category_id: category_id)
  end
end
