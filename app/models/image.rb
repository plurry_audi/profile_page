class Image < ActiveRecord::Base
  has_attached_file :file, :use_timestamp => false
  validates_attachment_content_type :file, :content_type => %w(image/jpeg image/jpg image/png)
  belongs_to :post
end
