class Story < ActiveRecord::Base
  has_many :story_images, :dependent => :destroy
  def self.create_story(params)
    story = Story.create(
        title: params[:title],
        subtitle: params[:subtitle],
        content: params[:content],
        lat: params[:lat],
        lng: params[:lng],
        date1: params[:date1],
        date2: params[:date2]
    )
    params[:file].each do |f|
      StoryImage.create(story_id: story.id, file: f)
    end

    story.set_thumbnail
  end

  def set_thumbnail
    self.thumbnail = self.story_images.first.file.url(:thumb)
    self.save
  end

  def year
    self.date1.split("/")[2].to_i
  end

  def created_date
    self.created_at.localtime.strftime("%B %d, %Y AT %H:%M %p")
  end

  def date1_md
    self.date1.split("/")[0..1].join('.')
  end

  def date2_md
    self.date2.split("/")[0..1].join('.')
  end
end
