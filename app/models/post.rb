class Post < ActiveRecord::Base
  include ActionView::Helpers::TextHelper

  has_many :images, :dependent => :destroy
  has_many :post_images, :dependent => :destroy
  has_many :comments

  def self.create_post(params)
    post = Post.create( title: params[:title],
                        content: params[:content],
                        major: params[:major],
                        minor: params[:minor] )
    unless params[:files].nil?
      params[:files].each do |file|
        PostImage.create(post_id: post.id, file: file)
      end
    end

    post
  end

  def connect_images(images)
    images.each do |image_id|
      Image.find(image_id).update(post_id: self.id, is_connected: true)
    end
  end

  def set_thumbnail
    self.update(thumbnail: self.post_images.first.file.url(:thumb))
  end

  def self.blog_posts
    Post.where(major: 'Blog').order('created_at desc')
  end

  def self.portfolio_posts
    Post.where(major: 'Portfolio').order('created_at desc')
  end

  def date
    self.created_at.localtime.strftime("%B %d, %Y AT %H:%M %p")
  end

  def summary
    truncate(strip_tags(self.content.gsub(/\r|\n/,'')), length: 150)
  end

  def modify(params)
    # delete image
    params[:delete].each do |post_image_id|
      PostImage.find(post_image_id.to_i).destroy unless post_image_id.empty?
    end
    # append image
    unless params[:files].nil?
      params[:files].each do |file|
        PostImage.create(post_id: self.id, file: file)
      end
    end
    self.update(title: params[:title], content: params[:content], major: params[:major], minor: params[:minor])
    self.set_thumbnail
  end

  def major_id
    Category.where(name: self.major).take.id
  end

  def next_post
    posts = Post.where(major: self.major)
    index = posts.find_index(self)
    is_last = (index == (posts.count-1))
    return nil if is_last
    posts[index + 1]
  end

  def prev_post
    posts = Post.where(major: self.major)
    index = posts.find_index(self)
    is_first = (index == 0)
    return nil if is_first
    posts[index - 1]
  end

  def increase_count
    self.view_count += 1
    self.save
  end
end
