class StoryImage < ActiveRecord::Base
  has_attached_file :file, :use_timestamp => false,
                    :styles => {
                      :thumb => "250x160^",
                      :storydetail => "1140x"
                    }
  validates_attachment_content_type :file, :content_type => %w(image/jpeg image/jpg image/png)
  belongs_to :story
end
