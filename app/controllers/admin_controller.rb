class AdminController < ApplicationController
  layout 'admin'
  before_action :session_check
  skip_before_action :session_check, only: [:login, :login_check]

  def session_check
    unless session[:is_logined]
      return redirect_to '/admin/login'
    end
  end

  def index
    @posts = Post.all
    @stories = Story.all
    @admin = Admin.find(session[:admin_id])
  end

  def login
  end

  def login_check
    admin = Admin.find_by(email: params[:email], password: params[:password])
    unless admin.nil?
      session[:is_logined] = true
      session[:admin_id] = admin.id
      return redirect_to '/admin'
    end

    redirect_to '/admin/login'
  end

  def logout
    session[:is_logined] = false
    session[:admin_id] = nil
    redirect_to '/admin'
  end

  def story
  end

  def story_proc
    story = Story.create_story(params)
    redirect_to '/admin'
  end

  def story_delete
    Story.destroy(params[:sid].to_i)
    redirect_to '/admin'
  end

  ############## POST ########################
  def clean_post_image
    Image.where(is_connected: false).destroy_all
    session[:images] = Array.new
    render :text => "ok"
  end

  def post
    @major_categories = Category.where(category_id: nil)
    session[:images] = Array.new
    
    @categories = Hash.new
    @major_categories.each do |mj|
      @categories[mj.name] = Category.where(category_id: mj.id).map {|c| c.name}
    end

    respond_to do |format|
      format.html
      format.json { render :json => @categories }
    end
  end

  def post_proc
    post = Post.create_post( params )
    post.connect_images(session[:images])
    post.set_thumbnail
    redirect_to '/admin'
  end

  def post_delete
    Post.destroy(params[:pid])
    redirect_to '/admin'
  end

  def post_modify
    unless params[:pid].nil?
      @post = Post.find(params[:pid])
      @minor_categories = Category.where(category_id: @post.major_id)
      session[:modify_post_id] = params[:pid]
    end
    
    @major_categories = Category.where(category_id: nil)

    @categories = Hash.new
    @major_categories.each do |mj|
      @categories[mj.name] = Category.where(category_id: mj.id).map {|c| c.name}
    end

    respond_to do |format|
      format.html
      format.json { render :json => @categories }
    end
  end

  def post_modify_proc
    Post.find(session[:modify_post_id]).modify( params )
    redirect_to '/admin'
  end
  
  def post_modify_get_images
    @post = Post.find(session[:modify_post_id])

    result = Array.new
    @post.post_images.each do |post_image|
      image = Hash.new
      image['id'] = post_image.id
      image['name'] = post_image.file_file_name
      image['size'] = post_image.file_file_size
      result.append(image)
    end

    render :json => result
  end
end
