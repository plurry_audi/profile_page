require 'rest-client'
class ContactController < ApplicationController
  def index
  end

  def send_mail
    RestClient.post "https://api:key-3ax6xnjp29jd6fds4gc373sgvjxteol0"\
    "@api.mailgun.net/v3/samples.mailgun.org/messages",
    :from => "#{params[:name]} <#{params[:email]}>",
    :to => "your@email.com",
    :subject => "Contact",
    :text => params[:content]
    redirect_to :back
  end
end
