class BlogController < ApplicationController
  def index
    @posts = Post.blog_posts
    @categories = Category.where(name: 'Blog').take.minor
  end
end
