class PortfolioController < ApplicationController
  def index
    @posts = Post.portfolio_posts
    @categories = Category.where(name: 'Portfolio').take.minor
  end
end
