class PostController < ApplicationController
  def index
    pid = params[:pid]
    @post = Post.find(pid)
    @current_url = request.url
    @comments = @post.comments

    @post.increase_count
  end

  def comment_write
    pid = params[:pid]
    name = params[:name]
    password = params[:password]
    content = params[:content]

    Comment.create( post_id: pid,
                    name: name,
                    password: password,
                    content: content
                  )
    redirect_to :back
  end
end
