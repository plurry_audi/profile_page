var scrollAnim = function() {
  $('body').animate({
    scrollTop: ($('.content-wrapper').offset().top)
  }, 500);
};
var scrollCheck = function () {
  if( $(window).scrollTop() == 0 ) {
    $('header').removeClass('show');
  } else {
    $('header').addClass('show');
  }
};
$(document).ready( function() {
  $('.down-scrolling img').click( scrollAnim );
  $('.down-post').click( scrollAnim );
  $(window).scroll( scrollCheck );
});
