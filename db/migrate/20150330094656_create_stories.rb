class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.string :title
      t.string :subtitle
      t.text :content
      t.string :thumbnail
      t.string :lat
      t.string :lng
      t.string :date1
      t.string :date2

      t.timestamps null: false
    end
  end
end
