class CreatePostImages < ActiveRecord::Migration
  def change
    create_table :post_images do |t|
      t.belongs_to :post
      t.attachment :file
      t.timestamps null: false
    end
  end
end
