class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.belongs_to :post
      t.attachment :file
      t.boolean :is_connected, default: false
      t.timestamps null: false
    end
  end
end
