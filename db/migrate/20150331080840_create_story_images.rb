class CreateStoryImages < ActiveRecord::Migration
  def change
    create_table :story_images do |t|
      t.belongs_to :story
      t.attachment :file
      t.timestamps null: false
    end
  end
end
