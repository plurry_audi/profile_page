# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Admin.create(email: 'admin@admin.com', name: 'My Name', password: 'adminpassword')

blog = Category.create(name: "Blog")
work = Category.create(name: "Portfolio")

Category.create(name: "Daily", category_id: blog.id)
Category.create(name: "IT", category_id: blog.id)
Category.create(name: "School", category_id: blog.id)
Category.create(name: "Choco", category_id: blog.id)

Category.create(name: "Interior", category_id: work.id)
Category.create(name: "Drawing", category_id: work.id)
Category.create(name: "Graphic", category_id: work.id)
Category.create(name: "Photograph", category_id: work.id)
